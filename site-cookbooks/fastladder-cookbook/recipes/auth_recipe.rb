include_recipe "fastladder-cookbook::deployer"

directory "/home/deployer/.ssh" do
	action :create
	owner "deployer"
	mode "0700"
end

cookbook_file "/home/deployer/.ssh/id_rsa" do
	action :create
	owner "deployer"
	mode "0600"
end

cookbook_file "/home/deployer/.ssh/authorized_keys" do
	action :create
	owner "deployer"
	mode "0600"
end