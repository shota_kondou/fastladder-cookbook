user 'deployer' do
	action :create
	supports :manage_home => true
	home '/home/deployer'
	shell "/bin/bash"
end